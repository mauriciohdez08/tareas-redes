SHELL=/bin/bash

PAYLOAD_DIR=content/tarea

.PHONY:	all clean

all:
	find ${PAYLOAD_DIR} -mindepth 2 -iname Makefile -printf '%h\0' \
	| xargs -0 -r -t -n 1 -I {} sh -c "$(MAKE) -C {} || true"

clean:
	find ${PAYLOAD_DIR} -mindepth 2 -type f -executable -print0 \
	| xargs -0 -r -n 1 -I {} \
	  sh -c "file '{}' | grep ELF >/dev/null && rm -v '{}' || true"
