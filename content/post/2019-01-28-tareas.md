---
title: Tareas
subtitle: Redes 2019-2
date: 2019-01-28
tags: ["tareas"]
---

En este espacio estaremos concentrando las tareas de la materia de [redes de computadoras][redes-gitlab] semestre 2019-2 impartida en la [Facultad de Ciencias, UNAM][redes-fciencias].


[redes-gitlab]: http://Redes-Ciencias-UNAM.gitlab.io/
[redes-fciencias]: http://www.fciencias.unam.mx/docencia/horarios/20192/1556/714
