#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include<fcntl.h>
#include<unistd.h>
#include"lista.c"
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>

void error(char *s);
void procesoArchivo(char *archivo);

int main() {
  struct lista nomArch = {cabeza:0, longitud:0};

  int entra = 1;
  do{
    int opcion;
    printf("1. Insertar archivo\n2. Leer archivo\n3. Eliminar archivo\n4. Imprimir archivos\n");
    scanf("%d", &opcion);
    switch (opcion) {
      case 1:

        printf("opcion 1\nIntroduce un nombre de archivo archivo: ");
        char *cad1 = (char*)malloc(sizeof(char)*100);
        scanf("%s", cad1);
        agrega_elemento(&nomArch, cad1);
        break;

      case 2:

        printf("opcion 2\n");
        int opcion2;
        int longitudLista2 = nomArch.longitud;
        if(longitudLista2 == 0){
            error("No hay elementos en la lista");
        }
        printf("Numero de archivo: ");
        scanf("%d",&opcion2);
        if(opcion2>=0 && opcion2<longitudLista2){

          char * cad2 = (char*)obten_elemento(&nomArch, opcion2);
          int existe = -1;
          FILE *fd;
          fd = fopen(cad2, "rt");//read text
          if(fd != NULL){
            int c;
              while((c=fgetc(fd))!=EOF){
                  existe = 1;
                  if(c == '\n'){
                      printf("\n");
                  }else{
                    putchar(c);
                  }
              }
              fclose(fd);
          }
          DIR *dir;
          struct dirent *ent;
          dir = opendir(cad2);
          if (dir != NULL){
              while ((ent = readdir (dir)) != NULL){
                  if ( (strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0) ){
                    existe = 1;
                  procesoArchivo(ent->d_name);
                }
              }
              closedir (dir);
        }
        if(existe==-1){
          error("El archivo no existe o el directorio no es valido");
        }
        }else{
          error("entrada fuera de rango");
        }
        break;

      case 3:
        printf("opcion 3\n");
        int longitudLista3 = nomArch.longitud;
        if(longitudLista3==0){
          error("No hay elementos en la lista");
        }
        printf("Indice de elemento a eliminar: ");
        int opcion3;
        scanf("%d",&opcion3);
        if(opcion3>=0 && opcion3<longitudLista3){
          char * e = (char *)elimina_elemento(&nomArch, opcion3);
          printf("Elemento eliminado: %s\n", e);
          free(e);
          break;
        }else{
          error("entrada fuera de rango");
        }

      case 4:
        printf("opcion 4\n");
        aplica_funcion(&nomArch, imprimeCadena);
        //imprime_todos_los_elementos(&nomArch);
        break;
      default:
        entra = 0;
        error("Opcion no valida.");

    }
  }while(entra);
  puts("");
}

void error(char *s){
  fprintf(stderr, " %s\n", s);
  exit(1);
}

void procesoArchivo(char *archivo) {
  FILE *fich;
  long ftam;

  fich=fopen(archivo, "r");
  if (fich) {
      fseek(fich, 0L, SEEK_END);
      ftam=ftell(fich);
      fclose(fich);
      /* Si todo va bien, decimos el tamaño */
      printf ("%s\n", archivo);
    }
  else
    /* Si ha pasado algo, sólo decimos el nombre */
    printf ("%30s (No info.)\n", archivo);
}
