﻿***
Práctica 1: Programación en C.
Rivera González Damián.
***

***
Contenido.

Dentro del la carpeta Practica01 se encuentran los archivos punto c, para compilar con Makefile. Archivos:

	- lista.c : contiene todas la funciones y definiciones para crear estructuras 
			de tipo lista y las funciones que estás pueden hacer.
	- archivos.c : se encuentra la función principal que se usará para el 					funcionamiento del menú.
	- auxiliares.c : contiene funciones que se usarán para leer directorios o 				leer archivos	
	- ejemplo.c : se encuentra el código de prueba que se muestra en el pdf de la  			descripción de la práctica para mostrar el funcionamiento de las 			listas implementadas

	- bsort.c: Se encuentra la implementación del ordenamiento burbuja mediante 				apuntadores.

	- etc.c : contiene el código fuente para el segundo punto extra.

	- Pruebas : una carpeta que únicamente es para la muestra del funcionamiento 			del manejo de archivos.

Funcionamiento.

Ejercicio1. Lista Enlazada.
Una vez que se haya ejecutado el comando ‘make’ para la compilación de todos los archivos necesarios. 
Para el primer ejercicio haremos la muestra de la ejecución de archivo ‘ejemplo’

	./lista_enlazada	

Y se mostrará la ejecución del ejemplo que contenía la descripción de la práctica.



Ejercicio2. Manejo de Archivos.
Ejecutamos el programa “manejo_archivos” y se correrá el menú. Con 5 Opciones para trabajar con ellas con la siguiente instrucción

	./manejo_archivos



Ejercicio Extra. Bubble sort.

Basta con ejecutar el archivo ./bsort e ingresar una serie de parámetros como en el ejemplo siguiente:

	./bsort 9 8 7 6 5 4 3 2 1

Y dar enter

Ejercicio extra. Etc-passwd.
Una vez que se haya ejecutado el comando de make, se generará un ejecutable “etc”
el cual se deberá ejecutar con la instrucción 

	./etc

Y dar enter para ejecutar.
