# Práctica 4

#### Hernández Olvera Mauricio Eduardo

![alt text](img/01_new_netconfig.png)

![alt text](img/02_server_config.png)

![alt text](img/03_prev_config.png)

![alt text](img/04_after_config.png)

![alt text](img/05_dns_config.png)

![alt text](img/06_ping.png)

![alt text](img/07_server_ip.png)

![alt text](img/08_disable_ipv6.png)

![alt text](img/09_disable_ipv6.png)

![alt text](img/10_client_netconfig.png)

![alt text](img/11_dhcp_packages.png)

![alt text](img/12_dhcp_packages.png)

![alt text](img/13_syslog.png)

![alt text](img/14_dhcp_leases.png)

![alt text](img/15_pxe_netconfig.png)
