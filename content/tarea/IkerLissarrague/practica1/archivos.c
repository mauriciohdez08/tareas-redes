#include "lista.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

void imprimeEntrada(void* elemento){
	char* e = (char*)elemento;
	printf("%s\n", e);
}

void imprimirContenido(char* ruta){
	FILE *fptr;
	char c;
    // Open file 
    fptr = fopen(ruta, "r"); 
    if (fptr == NULL)
        fprintf(stderr,"Hubo un error intentando abrir el archivo %s\n",ruta);   
    c=fgetc(fptr); 
    while (c != EOF) { 
        printf ("%c", c); 
        c = fgetc(fptr); 
    }
    fclose(fptr); 
    printf("\n");
}

void imprimirDirectorio(char* ruta){
    DIR* d;
    struct dirent* dir;
    d=opendir(ruta);
    if (d){
        while ((dir = readdir(d)) != NULL){
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
}

int esDirectorio(char* ruta,struct stat* s){
	if( stat(ruta,s) == 0 )
	    return s->st_mode & S_IFDIR;
	return 0;
}

int esArchivo(char* ruta,struct stat* s){
	if( stat(ruta,s) == 0 )
	    return s->st_mode & S_IFREG;
	return 0;
}

int esValido(char* ruta){
	struct stat s;
	return esDirectorio(ruta,&s)||esArchivo(ruta,&s);
    // FILE *file;
    // if (file = fopen(archivo, "r")){
    //     fclose(file);
    //     return 1;
    // }
    // return 0;
}

void agrega(struct lista* lista) {
	char ruta[101]="";
	printf("Ingresa el nombre del archivo a agregar. (se tomarán en cuenta a lo más 100 caracteres)\n");
	//fgets(ruta, 100, stdin);
	scanf("%100s", ruta);
	while(!esValido(ruta)){
		fprintf(stderr,"El archivo %s no es un archivo válido. Intentalo de nuevo.\n",ruta);
		printf("Ingresa el nombre del archivo a agregar. (se tomarán en cuenta a lo más 100 caracteres)\n");
		//fgets(ruta, 100, stdin);
		scanf("%100s", ruta);
	}
	char* p = (char*)malloc(sizeof(char) * strlen(ruta)+1);
	strcpy(p, ruta);
	agrega_elemento(lista,p);
	printf("Se ha agregado el archivo \'%s\'\n",p);
}

void leer(struct lista* lista){
	if(lista->longitud==0){
		fprintf(stderr,"Actualmente no hay archivos disponibles para su lectura. Intenta otra opción.\n");
		return;
	}
	int selection=0;
	if(lista->longitud==1)
		printf("Hay una única entrada \'%s\'. Se leera a continuación:\n",(char*)obten_elemento(lista,selection));
	else{
		printf("Ingresa el número de archivo entre 0 y %d a leer:\n",lista->longitud-1);
		scanf("%d",&selection);
		while(selection<0 || selection>=lista->longitud){
			fprintf(stderr,"El número %d no es válido. Intentalo de nuevo\n",selection);
			printf("Ingresa el número de archivo entre 0 y %d a leer:\n",lista->longitud-1);
			scanf("%d",&selection);	
		}
	}

	char* elem=(char*)obten_elemento(lista,selection);

	//sabemos que el contenido es válido
	//si es archivo, imprimimos su contenido,
	//de otra forma imprimimos los archivos en el directorio
	struct stat s;
	if(esArchivo(elem,&s))
		imprimirContenido(elem);
	else
		imprimirDirectorio(elem);

}

void elimina(struct lista* lista){
	if(lista->longitud==0){
		fprintf(stderr,"Actualmente no hay archivos a eliminar. Intenta otra opción.\n");
		return;
	}
	int selection=0;
	int selection2=0;
	if(lista->longitud==1)
		printf("Hay una única entrada y está ubicada en la posición 0.\n");
	else{
		printf("Ingresa el número de archivo entre 0 y %d a eliminar:\n",lista->longitud-1);
		scanf("%d",&selection);
		while(selection<0 || selection>=lista->longitud){
			fprintf(stderr,"El número %d no es válido. Intentalo de nuevo.\n",selection);
			printf("Ingresa el número de archivo entre 0 y %d a eliminar:\n",lista->longitud-1);
			scanf("%d",&selection);	
		}
	}
	printf("¿Estás seguro de querer eliminar la entrada \'%s\'?\n",(char*)obten_elemento(lista,selection));
	printf("Ingresa 1 para continuar la eliminación.\n");
	printf("Ingresa cualquier otra cosa para cancelar la eliminación.\n");
	scanf("%d",&selection2);
	if(selection2==1){
		char* e=elimina_elemento(lista,selection);
		printf("Se ha eliminado el archivo \'%s\'\n",e);
		free(e);
	}
	else
		printf("Se ha cancelado la eliminación del archivo \'%s\'\n",(char*)obten_elemento(lista,selection));
}

void imprime(struct lista* lista){
	if(lista->longitud==0)
		fprintf(stderr,"Actualmente no hay archivos a imprimir. Intenta otra opción.\n"); 
	else{
		printf("Los archivos son los siguientes:\n");
		aplica_funcion(lista,imprimeEntrada);
	}
}

int main(){
	int selection=-1;
	struct lista lista = {.cabeza=0, .longitud=0};
	while(selection!=0){
		printf("1. Insertar archivo\n2. Leer archivo\n3. Eliminar archivo\n4. Imprimir archivos\n");
		scanf("%d",&selection);
		switch(selection){
			case 1:
				agrega(&lista);
				break;
			case 2:
				leer(&lista);
				break;
			case 3:
				elimina(&lista);
				break;
			case 4:
				imprime(&lista);
				break;
			default:
				break;
		}
	}
}