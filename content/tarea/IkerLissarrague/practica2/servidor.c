#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>

void error(const char * msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[]){
	int server_fd,new_socket,conn_fd,port;
	unsigned int clilen;
	char sendBuff[1025];
	char line[101];
	char recvBuff[101];
	char str[INET_ADDRSTRLEN];
	struct sockaddr_in server,cli_addr;
	FILE *fp;
	if(argc==1)
		error("ERROR: Se debe de pasar como argumento el puerto a escuchar.");
	server_fd=socket(AF_INET, SOCK_STREAM, 0);
	if(server_fd<0)
		error("ERROR: No se pudo abrir Socket.");

	memset(sendBuff, '0', sizeof(sendBuff));
	port=atoi(argv[1]);
	server.sin_family=AF_INET;
	server.sin_addr.s_addr=INADDR_ANY;
	server.sin_port=htons(port);

	if(bind(server_fd,(struct sockaddr*)&server,sizeof(server))<0)
		error("ERROR: No se ha podido hacer la conexión.");

	if(listen(server_fd,3)<0)
		error("ERROR: No se ha podido escuchar el puerto.");

	//handle errors!
	clilen = sizeof(cli_addr);
	conn_fd = accept(server_fd, (struct sockaddr*)&cli_addr,&clilen);
	inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);
	printf("server: Conexión aceptada desde %s:%d\n",str,cli_addr.sin_port);

	while(strcmp(recvBuff,"EOF")!=0){
		bzero(recvBuff,sizeof(recvBuff));
		bzero(sendBuff,sizeof(sendBuff));
		bzero(line,sizeof(line));
		read(conn_fd,recvBuff,sizeof(recvBuff));
		if(strcmp(recvBuff,"EOF")==0)
			break;
		printf("client: %s\n",recvBuff);
		strcat(recvBuff," 2>&1");
		fp = popen(recvBuff,"r");
		while(fgets(line, sizeof(line), fp) != NULL)
			strcat(sendBuff,line);
		pclose(fp);
		write(conn_fd,sendBuff,strlen(sendBuff));
		// printf("Finished writing to client!\n");
	}

	close(conn_fd);
	printf("Se ha cerrado la conexión\n");

	// while(strcmp(recvBuff,'EOF')!=0){
	// 	conn_fd = accept(server_fd, (struct sockaddr*)&cli_addr,&clilen);
	// 	inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);
	// 	printf("server: Conexión aceptada desde %s:%d\n",str,cli_addr.sin_port);
	// 	// ticks = time(NULL);
	// 	// snprintf(sendBuff, sizeof(sendBuff), "%.24s\r\n", ctime(&ticks));
	// 	//use send and read!!!!!!!
	// 	read(conn_fd, recvBuff, sizeof(recvBuff)-1);
	// 	printf("%s\n",recvBuff);
	// 	send(conn_fd, sendBuff, strlen(sendBuff),0);
		
	// 	if(strcmp("EOF",recvBuff))
	// 		close(conn_fd);
	// }

}