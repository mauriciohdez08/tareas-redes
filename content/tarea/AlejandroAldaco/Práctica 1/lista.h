#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo
{
  void * elemento;
  struct nodo * siguiente;
};

struct lista
{
  struct nodo * cabeza;
  struct nodo * ultimo;
  int longitud;
};

void agrega_elemento(struct lista * lista, void * elemento)
{
  if (lista->cabeza == 0)
  {
    lista->cabeza = elemento;
    lista->cabeza->siguiente = NULL;
    lista->ultimo = elemento;
    lista->ultimo->siguiente = NULL;
  }
  else
  {
    lista->ultimo->siguiente = elemento;
    lista->ultimo = elemento;
    lista->ultimo->siguiente = NULL;
  }
  lista->longitud++;
}

void * obten_elemento(struct lista * lista, int n)
{
  if (n > lista->longitud) return 0;
  struct nodo * actual = malloc(sizeof(struct nodo));
  actual = lista->cabeza;
  for (int i = 0; i < n; i++)
  {
    actual = actual->siguiente;
  }
  return actual;
}

void * elimina_elemento(struct lista * lista, int n)
{
  if (n > lista->longitud-1)  return 0;
  struct nodo * actual = malloc(sizeof(struct nodo));
  actual = lista->cabeza;
  if (n == 0)
  {
    if (lista->cabeza->siguiente != NULL)
    {
      lista->cabeza = lista->cabeza->siguiente;
    }
    else
    {
      lista->cabeza = NULL;
      lista->ultimo = NULL;
    }
  }
  else
  {
    for (int i = 0; i < n-1; i++)
    {
      if (actual->siguiente->siguiente == NULL)
      {
        free(lista->ultimo);
        lista->ultimo = actual;
        lista->ultimo->siguiente = NULL;
      }
      else
      {
        struct nodo * elemento = malloc(sizeof(struct nodo));
        elemento = actual->siguiente;
        actual->siguiente = elemento->siguiente;
        actual = elemento;
        free(elemento);
      }
    }
  }
  lista->longitud--;
  return actual;
}

void aplica_funcion(struct lista * lista, void (*f)(void *))
{
  for (int i = 0; i < lista->longitud; i++)
  {
    (*f) (obten_elemento(lista,i));
  }
}
